class Airplane:
    """This is the Airplane class. Each Airplane in the app will be declared as one of these"""

    def __init__(self, ID, requestTime, requestedStart, duration):
        self.ID = ID
        self.requestTime = requestTime
        self.requestedStart = requestedStart
        self.duration = duration
        self.actualStart = -1

    def updateActualStart(self, actualStart):
        self.actualStart = actualStart

    def printInfo(self):
        return self.ID + " " + str(self.requestTime) + " " + str(self.requestedStart) + " " + str(self.actualStart)


class Terminal:
    """This is a class file with a list, where each Airplane is held after reading the import file"""

    def __init__(self):
        self.list = []

    def isEmpty(self):
        return len(self.list) < 1

    def addPlane(self, airplane):
        # if the list has no objects, just append it
        if len(self.list) == 0:
            self.list.append(airplane)
        # otherwise, loop through the list to add in correct index - based on requestedTime var
        else:
            insertIndex = len(self.list)
            for i in range(len(self.list)):
                if self.list[i].requestTime > airplane.requestTime:
                    insertIndex = i
            self.list.insert(insertIndex, airplane)

    # check if the front airplane in the list should take off at this time
    # if they should, remove them add them to the runway
    def removePlanes(self, runway, currentTime):
        removedPlanes = []
        for i in range(len(self.list)):
            if self.list[i].requestedStart <= currentTime:
                airplane = self.list[i]
                removedPlanes.append(airplane)
                runway.add(airplane)
        for j in range(len(removedPlanes)):
            self.list.remove(removedPlanes[j])

    def printList(self):
        for i in range(len(self.list)):
            print(self.list[i].ID)


class Runway:
    """This is the class file that holds a priority queue to act as  the runway."""

    def __init__(self):
        self.queue = []

    def isEmpty(self):
        return len(self.queue) < 1
    
    def add(self, airplane):
        # if the queue has no objects, just append it
        if len(self.queue) == 0:
            self.queue.append(airplane)
        # otherwise, loop through the queue to add in correct index - based on requestedStart var
        # ordered based on when each plane wants to leave - earlier requested start = higher priority
        # note, planes cannot request a time earlier than their submission time
        else:
            insertIndex = len(self.queue)
            for i in range(len(self.queue)):
                if self.queue[i].requestedStart > airplane.requestedStart:
                    insertIndex = i
            self.queue.insert(insertIndex, airplane)

    def updateFrontStart(self, currentTime):
        # if the front plane has started, update their actual start time
        if self.isEmpty() == False:
            if self.queue[0].actualStart == -1:
                self.queue[0].updateActualStart(currentTime)

    # this function will remove airplanes from the list if enough duraction has passed
    # compares their actual start time and duration to the current time
    def removeFinished(self, currentTime, finished):
        # checking only the front airplane
        if self.isEmpty() == False:
            front = self.queue[0]
            if (front.actualStart + front.duration) <= currentTime:
                self.queue.remove(front)
                finished.append(front)