from Classes import Airplane

# given the terminal object and file to read from
# create the airplanes and add them to the terminal
def readFile(terminal, fileName):
    inFile = open(fileName, 'r')
    for line in inFile:
        # this holds all of data from the line in list format
        lineData = line.rstrip().split(", ")
        newAirplane = Airplane(lineData[0], int(lineData[1]), int(lineData[2]), int(lineData[3]))
        terminal.addPlane(newAirplane)