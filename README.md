# Python Airport Simulator
An application that given a list of airport departures and flight durations, simulates the flight departure times and schedules when they can take off.

## Installation
Use the package manager [pip](https://pop.pypa.io/en/stable) to install the required libraries.
```bash
pip install example_library
```

## Usage
Run from the command line or in the Python shell.
```bash
$ python3 Main.py
```

## Directory Structure
```
+---DepartureTimes.txt
+---Requirements.txt
+---Main.py
+---FileManager.py
+---PrintManager.py
+---Classes.py
+---Simulator.py
+---Standard Design Document.docx
+---Standard Design Document.pdf
```