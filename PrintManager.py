def printSim(runway, currentTime):
  simPrint = "At time " + str(currentTime) + " the queue looks like:"
  for i in range(len(runway.queue)):
    simPrint += " " + runway.queue[i].ID
    if runway.queue[i].actualStart != -1:
      simPrint += " (started at " + str(runway.queue[i].actualStart) + ")"
    else:
      simPrint += " (scheduled for " + str(runway.queue[i-1].actualStart + runway.queue[i-1].duration) + ")"
  return simPrint

def printFinished(finished):
  printFin = ""
  for i in range(len(finished)):
    if i != 0:
      printFin += ", "
    printFin += finished[i].ID + " (" + str(finished[i].actualStart) + "-" + str(finished[i].actualStart + finished[i].duration - 1) + ")"
  
  return printFin