from FileManager import *
from PrintManager import *
from Classes import Terminal, Runway
from Simulation import *

def main():
    terminal = Terminal()
    runway = Runway()

    readFile(terminal, "DepartureTimes.txt")

    print("Printing The Terminal")
    terminal.printList()

    print("\nSimulation")
    finished = runSimulation(terminal, runway)

    print("\nPrinting Finished")
    print(printFinished(finished))

if __name__ == "__main__":
    main()