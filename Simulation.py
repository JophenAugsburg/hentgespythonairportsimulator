from PrintManager import *

def runSimulation(terminal, runway):
    finished = []

    time = 0
    while terminal.isEmpty() == False or runway.isEmpty() == False:
        # remove the front plane if it is done taking off
        runway.removeFinished(time, finished)
        # remove the planes from the terminal if their requested start is now
        # put the planes onto the runway in order of their requested start time
        terminal.removePlanes(runway, time)
        # update the actual start time if the front index (airplane) has not been set
        runway.updateFrontStart(time)

        print(printSim(runway, time))

        time += 1

    return finished
